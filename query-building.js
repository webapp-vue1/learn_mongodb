/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main() {
    // Update
    // const room = await Room.findById('623e2f3c67eb1e000a1135d9')
    // room.capacity = 20
    // room.save()
    // console.log(room)
    const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
    console.log(room)
    console.log('----------------')
    const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
    console.log(rooms)
    const buildings = await Building.find({})
    console.log(JSON.stringify(buildings))
}

main().then(() => {
    console.log('Finish')
})