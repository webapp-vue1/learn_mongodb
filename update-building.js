/* eslint-disable space-before-function-paren */
/* eslint-disable indent */
/* eslint-disable eol-last */
/* eslint-disable no-unused-vars */

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main() {
    const newInformaticsBuilding = await Building.findById('623e2f3c67eb1e000a1135d8')
    const room = await Room.findById('623e2f3c67eb1e000a1135dd')
    const informaticsBuilding = await Building.findById(room.Building)
    console.log(newInformaticsBuilding)
    console.log(room)
    console.log(informaticsBuilding)
    newInformaticsBuilding.rooms.push(room)
    informaticsBuilding.rooms.pull(room)
    room.save()
    newInformaticsBuilding.save()
    informaticsBuilding.save()
}

main().then(() => {
    console.log('Finish')
})